<?php

namespace Writer\Worksheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class CustomerWorksheet
 *
 * @author Jérôme Fath
 * @package Writer\Worksheet
 */
final class CustomerWorksheet  extends AbstractWorksheet
{
    /** @var string[] */
    const ROLES = ['ROLE_USER', 'ROLE_ADMIN'];

    /** @var string[] */
    const LOCKED = ['true', 'false'];

    /**
     * @inheritDoc
     */
    public static function countRow(): int
    {
        return 114;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Clients';
    }

    /**
     * @inheritDoc
     */
    public function populate(): Worksheet
    {
        $generator = $this->generator;

        $this
            ->generate(function($i) {
                return $i;
            })
            ->generate(function() use ($generator) {
                return $generator->email;
            })
            ->generate(function() use ($generator) {
                return md5($generator->password);
            })
            ->generate(function() use ($generator) {
                $roles = array_rand(array_flip(self::ROLES), rand(1, 2));
                return implode(',', is_array($roles) ? $roles : [$roles]);
            })
            ->generate(function() use ($generator) {
                return $generator->dateTimeThisYear()->format('Y-m-d H:i:s');
            })
            ->generate(function() use ($generator) {
                return array_rand(array_flip(self::LOCKED));
            })
        ;

        return $this->worksheet;
    }
}