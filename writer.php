<?php

include 'vendor/autoload.php';

$faker = Faker\Factory::create('fr_FR');
$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

$spreadsheet->removeSheetByIndex(0);
$spreadsheet->addSheet((new \Writer\Worksheet\CustomerWorksheet($faker, $spreadsheet))->populate());
$spreadsheet->addSheet((new \Writer\Worksheet\AddressWorksheet($faker, $spreadsheet))->populate());
$spreadsheet->addSheet((new \Writer\Worksheet\OrderWorksheet($faker, $spreadsheet))->populate());
$spreadsheet->addSheet((new \Writer\Worksheet\ProductWorksheet($faker, $spreadsheet))->populate());

$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
$writer->save("database.xlsx");
