<?php

namespace Writer\Worksheet;

use Closure;
use Faker\Generator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class AbstractWorksheet
 *
 * @author Jérôme Fath
 * @package Writer\Worksheet
 */
abstract class AbstractWorksheet implements WorksheetInterface
{
    /** @var Generator */
    protected $generator;

    /** @var Worksheet */
    protected $worksheet;

    /** @var int */
    protected $column = 0;

    /**
     * Worksheet constructor.
     *
     * @param Generator $generator
     * @param Spreadsheet $spreadsheet
     */
    public function __construct(Generator $generator, Spreadsheet $spreadsheet)
    {
        $this->generator = $generator;
        $this->worksheet = new Worksheet($spreadsheet, $this->getName());
    }

    /**
     * @param Closure $closure
     * @return WorksheetInterface
     */
    protected function generate(Closure $closure): WorksheetInterface
    {
        $this->column ++;

        for($i = 1; $i <= $this->countRow(); $i++) {
            $this->worksheet->setCellValueByColumnAndRow($this->column, $i, $closure($i));
        }

        return $this;
    }
}