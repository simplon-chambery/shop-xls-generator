<?php

namespace Writer\Worksheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class OrderWorksheet
 *
 * @author Jérôme Fath
 * @package Writer\Worksheet
 */
final class OrderWorksheet extends AbstractWorksheet
{
    /**
     * @inheritDoc
     */
    public static function countRow(): int
    {
        return 13478;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Ventes';
    }

    /**
     * @inheritDoc
     */
    public function populate(): Worksheet
    {
        $generator = $this->generator;

        $this
            ->generate(function() {
                return sprintf('customer-%d', mt_rand(1, CustomerWorksheet::countRow()));
            })
            ->generate(function() {
                return sprintf('product-%d', mt_rand(1, ProductWorksheet::countRow()));
            })
            ->generate(function() use ($generator) {
                return $generator->dateTimeThisYear()->format('Y-m-d H:i:s');
            })
            ->generate(function() use ($generator) {
                return mt_rand(1, 18);
            })
        ;

        return $this->worksheet;
    }
}