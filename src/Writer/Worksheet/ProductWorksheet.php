<?php

namespace Writer\Worksheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class ProductWorksheet
 *
 * @author Jérôme Fath
 */
final class ProductWorksheet extends AbstractWorksheet
{
    /** @var string[] */
    private static $names = [
        'Montre bracelet',
        'Portefeuille Homme  - Cuir',
        'Baby Phone vidéo vb601 - Sans fil',
        'T-shirt Star Wars',
        'Lampadaire trépied - H 148 cm',
    ];

    /**
     * @inheritDoc
     */
    public static function countRow(): int
    {
        return count(self::$names) * 3;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Produits';
    }

    /**
     * @inheritDoc
     */
    public function populate(): Worksheet
    {
        $generator = $this->generator;

        $this
            ->generate(function($i) {
                return $i;
            })
            ->generate(function() {
                return self::$names[mt_rand(0, sizeof(self::$names) - 1)];
            })
            ->generate(function() use ($generator) {
                return $generator->safeColorName;
            })
            ->generate(function() use ($generator) {
                return $generator->randomFloat(2, 50, 80);
            })
        ;

        return $this->worksheet;
    }
}