<?php

declare(strict_types=1);

namespace Writer\Worksheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class AddressWorksheet
 *
 * @author Jérôme Fath
 */
final class AddressWorksheet extends AbstractWorksheet
{
    /** @var string[] */
    const CITIES = [
        '73000' => 'Chambéry',
        '69000' => 'Lyon',
        '38000' => 'Grenoble',
        '74000' => 'Annecy'
    ];

    /** @var array */
    private $data;

    /**
     * @inheritDoc
     */
    public static function countRow(): int
    {
        return CustomerWorksheet::countRow() * 2;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'Adresses';
    }

    /**
     * @inheritDoc
     */
    public function populate(): Worksheet
    {
        $this
            ->generate(function($i) {
                $this->generateData($i);

                return $this->data[$i]['id'];
            })
            ->generate(function($i) {
                return sprintf('customer-%d', round($i/2));
            })
            ->generate(function($i) {
                return $this->data[$i]['company'];
            })
            ->generate(function($i) {
                return $this->data[$i]['firstName'];
            })
            ->generate(function($i) {
                return $this->data[$i]['lastName'];
            })
            ->generate(function($i) {
                return $this->data[$i]['phoneNumber'];
            })
            ->generate(function($i) {
                return $this->data[$i]['street'];
            })
            ->generate(function($i) {
                return $this->data[$i]['postCode'];
            })
            ->generate(function($i) {
                return $this->data[$i]['city'];
            })
            ->generate(function($i) {
                return $this->data[$i]['country'];
            })
            ->generate(function($i) {
                return $this->data[$i]['type'];
            })
        ;

        return $this->worksheet;
    }

    private function generateData(int $i)
    {
        $type = 'billing';

        if($i%2 === 1) {
            $this->data[$i] = $this->createAddress($i);
            $type = 'shipping';
        }
        else if(mt_rand(1, 10) === 1) {
            $this->data[$i] = $this->createAddress($i);
        }
        else {
            $this->data[$i] = $this->data[$i-1];
        }

        $this->data[$i]['type'] = $type;
    }

    private function createAddress(int $i): array
    {
        $postCode = array_rand(self::CITIES);

        return [
            'id' => $i,
            'company' => mt_rand(1, 10) === 1 ? $this->generator->company : null,
            'firstName' => $this->generator->firstName,
            'lastName' => $this->generator->lastName,
            'phoneNumber' => $this->generator->phoneNumber,
            'street' => $this->generator->streetAddress,
            'postCode' => $postCode,
            'city' => self::CITIES[$postCode],
            'country' => 'France'
        ];
    }
}