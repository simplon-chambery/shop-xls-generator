<?php

namespace Writer\Worksheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Interface WorksheetInterface
 *
 * @author Jérôme Fath
 * @package Writer\Worksheet
 */
interface WorksheetInterface
{
    /**
     * @return int
     */
    public static function countRow(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return Worksheet
     */
    public function populate(): Worksheet;
}